# DEBUG

Active development at [https://github.com/Baron-von-Riedesel/DOS-debug](https://github.com/Baron-von-Riedesel/DOS-debug)

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## DEBUG.LSM

<table>
<tr><td>title</td><td>DEBUG</td></tr>
<tr><td>version</td><td>2.50</td></tr>
<tr><td>entered&nbsp;date</td><td>2024-06-01</td></tr>
<tr><td>description</td><td>Program testing and editing tool</td></tr>
<tr><td>summary</td><td>A program testing and editing tool</td></tr>
<tr><td>keywords</td><td>32bit debugger, debug, debugger, DPMI debugger, kernel debugger</td></tr>
<tr><td>author</td><td>Paul Vojta &lt;vojta@math.berkeley.edu&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Andreas "Japheth" Grech &lt;mail -at- japheth.de&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/Baron-von-Riedesel/DOS-debug</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.japheth.de/Download/</td></tr>
<tr><td>platforms</td><td>DOS (assemble with NASM), FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[MIT License](LICENSE)</td></tr>
</table>
